#include "BinaryHeap.h"
#include <iostream>
#include <cstring>

using namespace std;

int main()
{
  BinaryHeap dij;
  Vertex current;
  Vertex *mapNew = new Vertex[10];
  for(int i = 0; i < 10; i++)
  {
    mapNew[i].index = i;
    mapNew[i].dist = i;
    mapNew[i].position.row = i;
    mapNew[i].position.col = i;
    mapNew[i].previous.row = i;
    mapNew[i].previous.col = i;
    mapNew[i].edges[0] = i;
    mapNew[i].edges[1] = i;
    mapNew[i].edges[2] = i;
    mapNew[i].edges[3] = i;
    mapNew[i].known = true; 
  }//for

  for(int i = 9; i >= 0; i--)
  {
    dij.insert(mapNew[i]);
  }  
   
  while(!dij.isEmpty())
  {
    dij.deleteMin(current);
    cout << current.position.row << ", " << current.position.col << endl;
  }
  return 1;
}

