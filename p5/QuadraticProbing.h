#ifndef _QUADRATIC_PROBING_H_
#define _QUADRATIC_PROBING_H_
#include <cstdio>

class ArtistSongs
{
public:
  char* name;
  int *songIndices;
  short songCount;
  ArtistSongs() : name(NULL), songCount(0){}
}; // class ArtistSongs

class QuadraticHashTable
{
  public:
    explicit QuadraticHashTable(int size);
    ArtistSongs& find(const char *artistName);
    void makeEmpty( );
    ArtistSongs& insert(char *artistName);
    int hashVal;
    ArtistSongs *array;
    int currentSize;
    int tableSize;
    int currentPos;
    int findPos( const char *artistName);
    int hash( const char *artistName);
};

#endif
