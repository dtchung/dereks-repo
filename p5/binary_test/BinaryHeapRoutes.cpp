#include "BinaryHeapRoutes.h"

/**
* Construct the binary heap.
* capacity is the capacity of the binary heap.
*/
BinaryHeapRoutes::BinaryHeapRoutes( )
  : currentSize(0), array( capacity + 1 )
{
}

/**
* Insert item x into the priority queue, maintaining heap order.
* Duplicates are allowed.
* Throw Overflow if container is full.
*/
void BinaryHeapRoutes::insert(Routes & x)
{
  if( isFull( ) )
    throw Overflow( );

  // Percolate up
  int hole = ++currentSize;
  for( ; hole > 1 && x.dist < array[ hole / 2 ].dist; hole /= 2 )
//    array[ hole ] = array[ hole / 2 ];
    copy(array[ hole ], array[ hole / 2 ]);

//  array[ hole ] = x;
  copy(array[ hole ], x);  	
}//insert

/**
* Find the smallest item in the priority queue.
* Return the smallest item, or throw Underflow if empty.
*/
const Routes & BinaryHeapRoutes::findMin( ) const
{
  if( isEmpty( ) )
    throw Underflow( );
  return array[ 1 ];
}

/**
* Remove the smallest item from the priority queue.
* Throw Underflow if empty.
*/
void BinaryHeapRoutes::deleteMin( )
{
  if( isEmpty( ) )
    throw Underflow( );

//   array[ 1 ] = array[ currentSize-- ];
    copy(array[ 1 ], array[ currentSize-- ]);  
   percolateDown( 1 );
}

/**
* Remove the smallest item from the priority queue
* and place it in minItem. Throw Underflow if empty.
*/
void BinaryHeapRoutes::deleteMin(Routes & minItem )
{
  if( isEmpty( ) )
    throw Underflow( );

  minItem = array[ 1 ];
  // array[ 1 ] = array[ currentSize-- ];
  copy(array[ 1 ], array[ currentSize-- ]);  
  percolateDown( 1 );
}

/**
* Establish heap order property from an arbitrary
* arrangement of items. Runs in linear time.
*/
void BinaryHeapRoutes::buildHeap( )
{
  for( int i = currentSize / 2; i > 0; i-- )
    percolateDown( i );
}

/**
* Test if the priority queue is logically empty.
* Return true if empty, false otherwise.
*/
bool BinaryHeapRoutes::isEmpty( ) const
{
  return currentSize == 0;
}

/**
* Test if the priority queue is logically full.
* Return true if full, false otherwise.
*/
bool BinaryHeapRoutes::isFull( ) const
{
  return currentSize == array.size( ) - 1;
}

/**
* Make the priority queue logically empty.
*/
void BinaryHeapRoutes::makeEmpty( )
{
  currentSize = 0;
}

/**
* Internal method to percolate down in the heap.
* hole is the index at which the percolate begins.
*/
void BinaryHeapRoutes::percolateDown( int hole )
{
/* 1*/ int child;
/* 2*/ Routes tmp = array[ hole ];

/* 3*/ for( ; hole * 2 <= currentSize; hole = child )
       {
/* 4*/   child = hole * 2;
/* 5*/   if( child != currentSize && array[ child + 1 ].dist < array[ child ].dist )
/* 6*/   child++;
/* 7*/ if( array[ child ].dist < tmp.dist )
// /* 8*/   array[ hole ] = array[ child ];
          copy(array[hole], array[child]); 
      else
/* 9*/   break;
       }
// /*10*/ array[ hole ] = tmp;
	   copy(array[hole], tmp);
}

void BinaryHeapRoutes::copy(Routes &x1, Routes &x2)
{
  x1.step = x2.step;
  x1.dist = x2.dist;

  for(int i = 0; i < x1.step; i++)
  {
    x1.route[i].row = x2.route[i].row;
    x1.route[i].col = x2.route[i].col;
  }
}


