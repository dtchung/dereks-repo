#include "BinaryHeap2.h"
#include <iostream>

using namespace std;
/**
* Construct the binary heap.
* capacity is the capacity of the binary heap.
*/
BinaryHeap2::BinaryHeap2( int capacity )
  : currentSize(0), array( capacity + 1 )
{
}

/**
* Insert item x into the priority queue, maintaining heap order.
* Duplicates are allowed.
* Throw Overflow if container is full.
*/
void BinaryHeap2::insert(Routes & x)
{
  if( isFull( ) )
    throw Overflow( );

  // Percolate up
  int hole = ++currentSize;
  for( ; hole > 1 && x.dist < array[ hole / 2 ].dist; hole /= 2 )
//    array[ hole ] = array[ hole / 2 ];
    copy(array[ hole ], array[ hole / 2 ]);

//  array[ hole ] = x;
  copy(array[ hole ], x);  	
}//insert

/**
* Find the smallest item in the priority queue.
* Return the smallest item, or throw Underflow if empty.
*/
const Routes & BinaryHeap2::findMin( ) const
{
  if( isEmpty( ) )
    throw Underflow( );
  return array[ 1 ];
}

/**
* Remove the smallest item from the priority queue.
* Throw Underflow if empty.
*/
void BinaryHeap2::deleteMin( )
{
  if( isEmpty( ) )
    throw Underflow( );

//   array[ 1 ] = array[ currentSize-- ];
    copy(array[ 1 ], array[ currentSize-- ]);  
   percolateDown( 1 );
}

/**
* Remove the smallest item from the priority queue
* and place it in minItem. Throw Underflow if empty.
*/
void BinaryHeap2::deleteMin(Routes & minItem )
{
  if( isEmpty( ) )
    throw Underflow( );

  minItem = array[ 1 ];
  // array[ 1 ] = array[ currentSize-- ];
  copy(array[ 1 ], array[ currentSize-- ]);  
  percolateDown( 1 );
}

/**
* Establish heap order property from an arbitrary
* arrangement of items. Runs in linear time.
*/
void BinaryHeap2::buildHeap( )
{
  for( int i = currentSize / 2; i > 0; i-- )
    percolateDown( i );
}

/**
* Test if the priority queue is logically empty.
* Return true if empty, false otherwise.
*/
bool BinaryHeap2::isEmpty( ) const
{
  return currentSize == 0;
}

/**
* Test if the priority queue is logically full.
* Return true if full, false otherwise.
*/
bool BinaryHeap2::isFull( ) const
{
  return currentSize == array.size( ) - 1;
}

/**
* Make the priority queue logically empty.
*/
void BinaryHeap2::makeEmpty( )
{
  currentSize = 0;
}

/**
* Internal method to percolate down in the heap.
* hole is the index at which the percolate begins.
*/
void BinaryHeap2::percolateDown( int hole )
{
/* 1*/ int child;
/* 2*/ Routes tmp = array[ hole ];

/* 3*/ for( ; hole * 2 <= currentSize; hole = child )
       {
/* 4*/   child = hole * 2;
/* 5*/   if( child != currentSize && array[ child + 1 ].dist < array[ child ].dist )
/* 6*/   child++;
/* 7*/ if( array[ child ].dist < tmp.dist )
// /* 8*/   array[ hole ] = array[ child ];
          copy(array[hole], array[child]); 
      else
/* 9*/   break;
       }
// /*10*/ array[ hole ] = tmp;
	   copy(array[hole], tmp);
}

void BinaryHeap2::copy(Routes &x1, Routes &x2)
{

  //delete x1.route;
  x1.route = new Coordinates[x2.step]; 
  for(int i = 0; i < x2.step; i++)
  {
    x1.route[i].row = x2.route[i].row;
    x1.route[i].col = x2.route[i].col;
  }
  
  x1.step = x2.step;
  x1.dist = x2.dist;
}


