#include "BinaryHeap2.h"
#include "BinaryHeap.h"
#include <iostream>
#include <cstring>

using namespace std;

int main()
{
  BinaryHeap2 krusk;
  Routes current;
  Routes *map = new Routes[10];
  for(int i = 0; i < 10; i++)
  {
    map[i].route = new Coordinates[10];
    map[i].step = 0;
    for(int j = 0; j < 10; j++)
    {
      map[i].route[j].row = i + j;
      map[i].route[j].col = i + j;
      map[i].step++;
    }
    map[i].dist = i;
        
  }//for

  for(int i = 0; i < 10; i++)
  {
    cout << "i: " << i << endl;
    krusk.insert(map[i]);
  }
  
   
  while(!krusk.isEmpty())
  {
    krusk.deleteMin(current);
    for(int i = 0; i < 10; i++)
    {
      cout << current.route[i].row << ", " << current.route[i].col << " | ";
    }
    cout << endl << "dist: " << current.dist << endl;
  }

  return 1;
}

