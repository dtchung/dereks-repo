#include <iostream>
#include <cstring>
#include "QuadraticProbing.h"
using namespace std;

/**
 * Construct the hash table.
 */

QuadraticHashTable::QuadraticHashTable(int size )
  : currentSize(0), tableSize(size)
{
  array = new ArtistSongs[tableSize];
}

/**
 * Insert item x into the hash table. If the item is
 * already present, then do nothing.
 */

ArtistSongs& QuadraticHashTable::insert(char*  x )
{
        // Insert x as active
    currentPos = findPos( x );
    if(array[currentPos].name == NULL)
    {
      currentSize++;
      array[currentPos].name = new char[strlen(x) + 1];
      strcpy(array[currentPos].name, x);
    }
    return array[currentPos];
}


/**
 * Method that performs quadratic probing resolution.
 * Return the position where the search for x terminates.
 */

int QuadraticHashTable::findPos( const char*  x )
{
/* 2*/      int currentPos = hash( x );

/* 3*/      while( array[ currentPos ].name != NULL &&
              strcmp(array[currentPos].name, x) != 0) // array[ currentPos ].hashVal != hashVal)
    {
              
/* 4*/          currentPos++;  // Compute ith probe
/* 5*/          if( currentPos >= tableSize)
/* 6*/              currentPos -= tableSize;
    }
/* 7*/      return currentPos;
}


/**
 * Find item x in the hash table.
 * Return the matching item, or ITEM_NOT_FOUND, if not found.
 */

ArtistSongs& QuadraticHashTable::find( const char*  x ) 
{
    currentPos = findPos( x );
    return array[currentPos];
}

/**
 * Make the hash table logically empty.
 */


int QuadraticHashTable::hash( const char *key)
{
    hashVal = 0;

    for( int i = 0; key[i]; i++ )
        hashVal = 37 * hashVal + key[ i ];

    hashVal %= tableSize;
    if( hashVal < 0 )
        hashVal += tableSize;

    return hashVal;
}

