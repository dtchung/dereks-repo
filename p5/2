// Author Sean Davis
//#include "router.h"
#include <iostream>
#include <limits.h>
#include <math.h>
#include "BinaryHeap.h"

using namespace std;

Router::Router(short **map, int width)
{
  
  mapNew = new Vertex*[width];
  copyMap(map, width);
  mapWidth = width;

} // Router()

void Router::copyMap(short **map, int width)
{

  for(int i = 0; i < width; i++)
    mapNew[i] = new Vertex[width];
  
  //fills in position, index, known, dist into mapNew
  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < width; j++)
    {
      mapNew[i][j].index = map[i][j];
      mapNew[i][j].known = false;
      mapNew[i][j].dist = INT_MAX;
      mapNew[i][j].position.row = i;
      mapNew[i][j].position.col = j;
    }//for j
  }//for i

  //fills in the edges
  for(int r = 0; r < width; r++)
  {
    for(int c = 0; c < width; c++)
    {
      //check east
      if(c != width - 1)
      {
        mapNew[r][c].edges[1] = (pow(mapNew[r][c].index - mapNew[r][c + 1].index, 2) + 10);
        mapNew[r][c + 1].edges[3] = mapNew[r][c].edges[1];
      }//if check east
      //check north
      if(r != 0)
      {
        mapNew[r][c].edges[0] = (pow(mapNew[r][c].index - mapNew[r - 1][c].index, 2) + 10);
        mapNew[r - 1][c].edges[2] = mapNew[r][c].edges[0];
      }//if check north
    }//for r
  }//for c
}//copyMap


void Router::findRoutes(const Coordinates *cityPos, int cityCount, Coordinates **paths, int pathCounts[])
{
  //array of cities, num of cities, paths and path counts
  int city_completed = 0;
  while (city_completed < cityCount - 1)
  {  
    dijkstra(cityPos, cityCount, paths, pathCounts, city_completed);
    city_completed++;
  }
    
}


void Router::dijkstra(const Coordinates *cityPos, int cityCount, Coordinates **paths, int pathCounts[], int city_completed)
{
  Coordinates current;
  Vertex temp;
  bool foundAll = false;//found all the cities
  int sum;
  BinaryHeap dij;
  int city_dij = 0;//for paths to count # of cities completed 
  //begin dijkstras
  if(city_completed < cityCount)
  {
    current.row= mapNew[cityPos[city_completed].row][cityPos[city_completed].col].position.row;
    current.col= mapNew[cityPos[city_completed].row][cityPos[city_completed].col].position.col;
    mapNew[current.row][current.col].known = true;
    mapNew[current.row][current.col].previous.row = -1;
    mapNew[current.row][current.col].previous.col = -1;
    mapNew[current.row][current.col].dist = 0;
  }//if city_completed < cityCount

  while(!foundAll)
  {
    //set adjacent cities
 
    //set north
    if(current.row != 0 && !mapNew[current.row - 1][current.col].known)
    {
      sum = (mapNew[current.row][current.col].dist + mapNew[current.row][current.col].edges[0]);
      if(mapNew[current.row - 1][current.col].dist > sum)
      {
        mapNew[current.row - 1][current.col].dist = sum;
        mapNew[current.row - 1][current.col].previous.row = mapNew[current.row][current.col].position.row;
        mapNew[current.row - 1][current.col].previous.col = mapNew[current.row][current.col].position.col;
 	//insert into heap
        dij.insert(mapNew[current.row - 1][current.col]);
      }//update distance
    }//set north
    

    //set east 
    if(current.col != mapWidth && !mapNew[current.row][current.col + 1].known)
    {
      sum = (mapNew[current.row][current.col].dist + mapNew[current.row][current.col].edges[1]);
      if(mapNew[current.row][current.col + 1].dist > sum)
      {
        mapNew[current.row][current.col + 1].dist = sum;
        mapNew[current.row][current.col + 1].previous.row = mapNew[current.row][current.col].position.row;
        mapNew[current.row][current.col + 1].previous.col = mapNew[current.row][current.col].position.col;
        //insert into heap
        dij.insert(mapNew[current.row][current.col + 1]);
         
      }//update distance
    }//set east

    //set south  
    if(current.row != mapWidth && !mapNew[current.row + 1][current.col].known)
    {
      sum = (mapNew[current.row][current.col].dist + mapNew[current.row][current.col].edges[2]);
      if(mapNew[current.row + 1][current.col].dist > sum)
      {
        mapNew[current.row + 1][current.col].dist = sum;
        mapNew[current.row + 1][current.col].previous.row = mapNew[current.row][current.col].position.row;
        mapNew[current.row + 1][current.col].previous.col = mapNew[current.row][current.col].position.col;
       
        dij.insert(mapNew[current.row + 1][current.col]);
        
      }//update distance
    }//south

    //set west  
    if(current.col != 0 && !mapNew[current.row][current.col - 1].known)
    {
      sum = (mapNew[current.row][current.col].dist + mapNew[current.row][current.col].edges[3]);
      if(mapNew[current.row][current.col - 1].dist > sum)
      {
        mapNew[current.row][current.col - 1].dist = sum;
        mapNew[current.row][current.col - 1].previous.row = mapNew[current.row][current.col].position.row;
        mapNew[current.row][current.col - 1].previous.col = mapNew[current.row][current.col].position.col;
        //insert into heap
        dij.insert(mapNew[current.row][current.col - 1]);
      }//update distance
    }//set west

    //pop from heap
    do
    {
      dij.deleteMin(temp);
    }while(mapNew[temp.position.row][temp.position.col].known);
   
    current.row = temp.position.row;
    current.col = temp.position.col;
    
    mapNew[current.row][current.col].known = true;
    
    //if current is position of a city
    for(int i = city_completed; i < cityCount; i++)
    {
      if(current.row == cityPos[i].row && current.col == cityPos[i].col)
      {
        Coordinates back;
        back.row = current.row;
        back.col = current.col;
        while(back.row != -1 || back.col != -1)
        {
          back.row = mapNew[back.row][back.col].previous.row; 
          back.col = mapNew[back.row][back.col].previous.col;
          paths[city_dij][pathCounts[city_dij]].row = back.row;
          paths[city_dij][pathCounts[city_dij]].col = back.col;
	  pathCounts[city_dij]++;
        }
	city_dij++;
      }
    }
    //check if all cities are completed
    for(int i = city_completed; i < cityCount; i++)
    {
      if(mapNew[cityPos[i].row][cityPos[i].col].known == false)
        break;

      if(i == cityCount - 1)
        foundAll = true;
    }
  }//while cities haven't been found yet

}//dijketra

//Router::backtrack(Coordinates current, 






























void Router::printRoutes()
{
  for (int i = 0; i < mapWidth; i++)
  {
    for (int j = 0; j < mapWidth; j++)
      cout << mapNew[i][j].index << " " ;
    cout << endl; 
  }

  cout << endl;

  cout << endl;
  cout << endl;

  for (int i = 0; i < mapWidth; i++)
  {
    if (i != 0)//prints north edges
    {    
      for (int j = 0; j < mapWidth; j++)
      {
        cout << mapNew[i][j].edges[0] << " " ;
      }//for
      cout << endl;
    }//if


    for (int j = 0; j < mapWidth; j++)//prints east edges
    {
      if(j != mapWidth - 1)
        cout << mapNew[i][j].edges[1] << " " ;
    }//for
    cout << endl; 
  }

  cout << endl;
  cout << endl;
  cout << endl;

  for (int i = 0; i < mapWidth; i++)
  {
      for (int j = 0; j < mapWidth; j++)
      {
        if(j != 0)
          cout << mapNew[i][j].edges[3] << " " ;
      }//for
      cout << endl;

    if(i != mapWidth - 1)
    {
      for (int j = 0; j < mapWidth; j++)//prints sout
      {
        //if(j != mapWidth - 1)
          cout << mapNew[i][j].edges[2] << " " ;
      }//for
      cout << endl;
    }//if 
  }
}//printRoutes()

//void dijkstra(
/*
void dijkstra(Coordinates)
{
  minHeap mh;
  Plot* plot = new Plot();
  Coordinates 
  mh.push( // push starting position [0][0]
  
  while(mh.empty() == false) 
  {
    top = mh.top();
    mh.pop();

    // check if visited
    if(visited[]) continue;
    visited[] = true;

    if(

  }

} // findRoutes()
*/

