// Author: Sutdents should put their names here.

#ifndef ROUTER_H
#define ROUTER_H

#include "RouterRunner.h"
#include "BinaryHeapRoutes.h"
using namespace std;

class Vertex // 
{
  public:
    Coordinates position;
    int index;//vertex number
    int dist; // distance
    bool known; 
    Coordinates previous;
    int edges[4];// 0 = north, 1 = east, 2 = south, 3 = west
};

/*
typedef struct{
  int row;
  int col;
} Coordinates;
*/

class Routes
{
  public:
    Coordinates *route;
    int dist;//distance of the route
    int step;//number of edges
};

class Router
{
  short mapWidth;
  Vertex **mapNew;
  Routes shortestPath;
  BinaryHeapRoutes krusk;
  public:
    Router(short **map, int width);
    void copyMap(short **map, int width); 
    void findRoutes(const Coordinates *cityPos, int cityCount, Coordinates **paths, int pathCounts[]); 
    void dijkstra(const Coordinates *cityPos, int cityCount, Coordinates **paths, int pathCounts[], int city_completed);
    void printRoutes();
    void clean_shortestPath();
    void resetMap();

};


#endif
