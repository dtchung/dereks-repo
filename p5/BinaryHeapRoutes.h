        #ifndef _BINARY_HEAP_ROUTES_H_
        #define _BINARY_HEAP_ROUTES_H_

        #include "dsexceptions.h"
        #include "vector.h"
//	#include "RouterRunner.h"
	#include "router.h"
        // BinaryHeapRoutes class
        //
        // CONSTRUCTION: with a negative infinity sentinel and
        //               optional capacity (that defaults to 100)
        //
        // ******************PUBLIC OPERATIONS*********************
        // void insert( x )       --> Insert x
        // deleteMin( minItem )   --> Remove (and optionally return) smallest item
        // Comparable findMin( )  --> Return smallest item
        // bool isEmpty( )        --> Return true if empty; else false
        // bool isFull( )         --> Return true if full; else false
        // void makeEmpty( )      --> Remove all items
        // ******************ERRORS********************************
        // Throws Underflow and Overflow as warranted
/*
class Vertex // 
{
  public:
    Coordinates position;
    int index;//vertex number
    int dist; // distance
    bool known; 
    Coordinates previous;
    int edges[4];// 0 = north, 1 = east, 2 = south, 3 = west
};
*/
class BinaryHeapRoutes
{
  public:
     explicit BinaryHeapRoutes( int capacity = 100 );
     bool isEmpty( ) const;
     bool isFull( ) const;
     const Routes & findMin( ) const;
     void insert( Routes & x );
     void deleteMin( );
     void deleteMin( Routes & minItem );
     void makeEmpty( );

  private:
    int currentSize;  // Number of elements in heap
    vector<Routes> array;        // The heap array
    void buildHeap( );
    void percolateDown( int hole );
    void copy(Routes &x1, Routes &x2);
 };

//#include "BinaryHeap.cpp"
#endif
