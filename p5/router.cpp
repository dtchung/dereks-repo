// Author Lewis Chi and Derek Chung
//#include "router.h"

/*
 * This program will take in a map and a list of city coordinates. It will then create
 * edges using the values of the coordinates. It will then use dijkstras to find the 
 * shortest path from one city to the rest of the other cities. Dijkstras will run N - 1
 * times in which N is the number of cities. It is unnecessary to run Dijktras again on
 * the previous city because the shortest path is already found. After finding each shortest
 * path, this program will get minimum spanning tree using a heap containing all the 
 * shortest paths. It will use Kruskaels to find all the shortest paths and it would use 
 * a disjoint union to make sure that the paths from each city would not be repeated. After
 * finding the paths, it would return the coordinates of the minimum spanning tree in a 
 * double array conatining coordinate classes (a coordinate class contains two ints for 
 * the row and column it is located on). This program is written based on the proffessor's 
 * test in which map size would be 350x350. The goal of the program is to attempt to match
 * the efficiency of the professor's program.
 * 
 *
 */

#include <iostream>
#include <limits.h>
#include <math.h>
#include "BinaryHeap.h"
#include "BinaryHeap2.h"
#include "DisjSets.h"
using namespace std;

/*
 * Constructor will copy over a map because the copy of the map would 
 * later be deleted in RouterRunner.cpp. This will also allocate space for
 * the Coordinate array in shortestPath class. 
 */
Router::Router(short **map, int width)
{
  
  mapNew = new Vertex*[width];
  copyMap(map, width);
  mapWidth = width;
  shortestPath.route = new Coordinates[mapWidth * mapWidth];
  shortestPath.step = 0;
} // Router()

/*
 * This function copies over the shorts in the map class and also generates the values
 * for the edges of each coordinate and gives each variable in the Vertex class an initial
 * value:
 *  Coordinates position = current coordinates
 *  int index = value on map
 *  int dist = 0;
 *  bool known = false;
 */
void Router::copyMap(short **map, int width)
{

  for(int i = 0; i < width; i++)
    mapNew[i] = new Vertex[width];
  
  //fills in position, index, known, dist into mapNew
  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < width; j++)
    {
      mapNew[i][j].index = map[i][j];
      mapNew[i][j].known = false;
      mapNew[i][j].dist = INT_MAX;
      mapNew[i][j].position.row = i;
      mapNew[i][j].position.col = j;
    }//for j
  }//for i

  //fills in the edges
  for(int r = 0; r < width; r++)
  {
    for(int c = 0; c < width; c++)
    {
      //check east
      if(c != width - 1)
      {
        mapNew[r][c].edges[1] = (pow(mapNew[r][c].index - mapNew[r][c + 1].index, 2) + 10);
        mapNew[r][c + 1].edges[3] = mapNew[r][c].edges[1];
      }//if check east
      //check north
      if(r != 0)
      {
        mapNew[r][c].edges[0] = (pow(mapNew[r][c].index - mapNew[r - 1][c].index, 2) + 10);
        mapNew[r - 1][c].edges[2] = mapNew[r][c].edges[0];
      }//if check north
    }//for r
  }//for c
}//copyMap


/*
 * This program will recieve a const Coordinates array which contains an array of two ints that contains the
 * the coordinates of each city, cityCount which is the number of cities, paths which contains a double array
 * of paths (RouterRunner.cpp will use paths to determine if the program is running) where we would be storing
 * the coordinates of the minimum spanning tree. pathCounts[] contains how long each path is in previous array.
 *
 * This program will first call Dijkstras N - 1 times (where N is the number of cities). It will continuously
 * call dijkstras until all but the last city has Dijkstras has been called (It is unnecessary to find the 
 * shortest routes from the last city because all the paths have been found). The next step is to use a disjoint
 * set and a minimum heap that will assist us in using Kruskael's algorithm. We will continuously pop from the 
 * heap and use the disjoint sets to determine if two cities have already been connected until all the cities
 * have been connected. If the cities have not been connected yet, we will store the coordinates into paths. 
 */
void Router::findRoutes(const Coordinates *cityPos, int cityCount, Coordinates **paths, int pathCounts[])
{
  //array of cities, num of cities, paths and path counts
  //cout << "findRoutes" << endl;
  int city_completed = 0;
  int count = 0;
  Routes temp;
  int city1, city2;

  while (city_completed < cityCount)
  {  
    //cout << "dijsktra #" << city_completed << endl;
    dijkstra(cityPos, cityCount, paths, pathCounts, city_completed);
    //cout << "call resetMap();" << endl;
    resetMap();
    city_completed++;
  }

  DisjSets set = DisjSets(cityCount);
  while(count < cityCount - 1)
  {
    krusk.deleteMin(temp);
   
    //find index in cityPos
    for(int i = 0; i < cityCount; i++)
    {
      //find city1
      if(cityPos[i].row == temp.route[0].row && cityPos[i].col == temp.route[0].col)
      {
        city1 = i;
        //cout << "Set city1" << endl;
      }
     
      if(cityPos[i].row == temp.route[temp.step - 1].row && cityPos[i].col == temp.route[temp.step - 1].col)
      {
        city2 = i;
        //cout << "set city2" << endl;
      }
    }//for
    //cout << "city 1: " << city1 << endl;
    //cout << "city 2: " << city2 << endl;
    //if(
    //set.unionSets(city1, city2);
    if(count == 0 || (set.find(city1) != set.find(city2)))
    {
      //cout << "set.find(city1): " << set.find(city1) << endl;
      //cout << "set.find(city2): " << set.find(city2) << endl;
      set.unionSets(set.find(city1), set.find(city2));
      //cout << "path[" << count << "]: ";
      for(int i = 0; i < temp.step; i++)
      {
        paths[count][i].row = temp.route[i].row;
        paths[count][i].col = temp.route[i].col;
        //pathCounts[count]++;
        //cout <<" (" << paths[count][i].row << ", " << paths[count][i].col << "), ";
      }//for
      //cout << endl << endl << endl << endl;
      pathCounts[count] = temp.step;
      count++; 
    }//if
  }//while
}//findRoutes

/*
 * This program will recieve a const Coordinates array which contains an array of two ints that contains the
 * the coordinates of each city, cityCount which is the number of cities, paths which contains a double array
 * of paths (RouterRunner.cpp will use paths to determine if the program is running) where we would be storing
 * the coordinates of the minimum spanning tree. pathCounts[] contains how long each path is in previous array,
 * and it would also read in city_completed which is the number of cities that have done dijkstras. The reason 
 * we read in city_completed is because it is unnecessary to find dijkstra from city B to A if you already did 
 * dijkstras from A to the rest of the cities. Therefore, to save time, the program doesn't run it twice
 *
 * We first start from one city and find the total distance of the coordinates around it then insert into a heap that 
 * will be set up based upon the total distance from each coordinate. The heap will later pop the coordiante with the least
 * distance and it will set that coordinate to known. It will check if the coordinate that it popped is a city and 
 * will increment the found city count later on. If the reached coordinate is a city, the program will backtrack based on the
 * coordinate value of the previous variable inside the vertex class. It will backtrack and later insert the cities into
 * shortestRoutes array.After all the cities have been reached the program will reset the map (since some varaibles have been
 * altered). 
 */
void Router::dijkstra(const Coordinates *cityPos, int cityCount, Coordinates **paths, int pathCounts[], int city_completed)
{
  //cout << "enter Dijkstra" << endl;
  Coordinates current;
  Vertex temp;
  bool foundAll = false;//found all the cities
  int sum;
  BinaryHeap dij;
  //BinaryHeapRoutes krusk;
  //int city_dij = 0;//for paths to count # of cities completed 
  //begin dijkstras
  //if(city_completed < cityCount)
  {
    current.row= mapNew[cityPos[city_completed].row][cityPos[city_completed].col].position.row;
    current.col= mapNew[cityPos[city_completed].row][cityPos[city_completed].col].position.col;
    mapNew[current.row][current.col].known = true;
    mapNew[current.row][current.col].previous.row = -1;
    mapNew[current.row][current.col].previous.col = -1;
    mapNew[current.row][current.col].dist = 0;
    //cout << "starting point: " << current.row << ", " << current.col << endl;
  }//if city_completed < cityCount

  while(!foundAll)
  {
    //cout << "enter while loop" << endl;
    //set adjacent cities
 
    //set north
    if(current.row != 0 && !mapNew[current.row - 1][current.col].known)
    {
      sum = (mapNew[current.row][current.col].dist + mapNew[current.row][current.col].edges[0]);
      if(mapNew[current.row - 1][current.col].dist > sum)
      {
        mapNew[current.row - 1][current.col].dist = sum;
        mapNew[current.row - 1][current.col].previous.row = mapNew[current.row][current.col].position.row;
        mapNew[current.row - 1][current.col].previous.col = mapNew[current.row][current.col].position.col;
 	//insert into heap
        dij.insert(mapNew[current.row - 1][current.col]);
      }//update distance
    }//set north
    //cout << "set north" << endl;

    //set east 
    if(current.col != mapWidth - 1 && !mapNew[current.row][current.col + 1].known)
    {
      sum = (mapNew[current.row][current.col].dist + mapNew[current.row][current.col].edges[1]);
      if(mapNew[current.row][current.col + 1].dist > sum)
      {
        mapNew[current.row][current.col + 1].dist = sum;
        mapNew[current.row][current.col + 1].previous.row = mapNew[current.row][current.col].position.row;
        mapNew[current.row][current.col + 1].previous.col = mapNew[current.row][current.col].position.col;
        //insert into heap
        dij.insert(mapNew[current.row][current.col + 1]);
         
      }//update distance
    }//set east
    //cout << "set east" << endl;

    //cout << "south set: " << current.row + 1 << ", " << current.col << endl;
    //set south  
    if(current.row != mapWidth - 1 && !mapNew[current.row + 1][current.col].known)
    {
      sum = (mapNew[current.row][current.col].dist + mapNew[current.row][current.col].edges[2]);
      if(mapNew[current.row + 1][current.col].dist > sum)
      {
        mapNew[current.row + 1][current.col].dist = sum;
        mapNew[current.row + 1][current.col].previous.row = mapNew[current.row][current.col].position.row;
        mapNew[current.row + 1][current.col].previous.col = mapNew[current.row][current.col].position.col;
       
        dij.insert(mapNew[current.row + 1][current.col]);
        
      }//update distance
    }//south
    //cout << "set south" << endl;

    //set west  
    if(current.col != 0 && !mapNew[current.row][current.col - 1].known)
    {
      sum = (mapNew[current.row][current.col].dist + mapNew[current.row][current.col].edges[3]);
      if(mapNew[current.row][current.col - 1].dist > sum)
      {
        mapNew[current.row][current.col - 1].dist = sum;
        mapNew[current.row][current.col - 1].previous.row = mapNew[current.row][current.col].position.row;
        mapNew[current.row][current.col - 1].previous.col = mapNew[current.row][current.col].position.col;
        //insert into heap
        dij.insert(mapNew[current.row][current.col - 1]);
      }//update distance
    }//set west
    //cout << "set west" << endl;
    
    //pop from heap
    do
    {
      dij.deleteMin(temp);
      //cout << "pop from heap" << endl;
    }while(mapNew[temp.position.row][temp.position.col].known);
   
    current.row = temp.position.row;
    current.col = temp.position.col;
    mapNew[current.row][current.col].known = true;
    
    //cout << "set to true: " << current.row << ", " << current.col << endl;
    //cout << "dist: " << mapNew[current.row][current.col].dist << endl;
     
    //if current is position of a city
    for(int i = city_completed; i < cityCount; i++)
    {
      if(current.row == cityPos[i].row && current.col == cityPos[i].col)
      {
        //cout << "current is city" << endl;
        shortestPath.dist = mapNew[current.row][current.col].dist;
        Coordinates back;
        back.row = current.row;
        back.col = current.col;
        while(back.row != -1 || back.col != -1)
        {
          //cout << "insert path: " << back.row << ", " << back.col << endl;
          shortestPath.route[shortestPath.step].row = back.row;
	        shortestPath.route[shortestPath.step].col = back.col;
          back.row = mapNew[shortestPath.route[shortestPath.step].row][shortestPath.route[shortestPath.step].col].previous.row;
          back.col = mapNew[shortestPath.route[shortestPath.step].row][shortestPath.route[shortestPath.step].col].previous.col;
	        shortestPath.step++;
	        /*
	        paths[city_dij][pathCounts[city_dij]].row = back.row;
          paths[city_dij][pathCounts[city_dij]].col = back.col;
          back.row = mapNew[paths[city_dij][pathCounts[city_dij]].row][paths[city_dij][pathCounts[city_dij]].col].previous.row;
	        back.col = mapNew[paths[city_dij][pathCounts[city_dij]].row][paths[city_dij][pathCounts[city_dij]].col].previous.col;
          pathCounts[city_dij]++;
	        */
	        //cout << "pathCounts[city_dij]: " << pathCounts[city_dij] << endl;
        }//while
	      //insert into heap
	      krusk.insert(shortestPath);
	      //clean_shortestPath();
	      shortestPath.step = 0;
	//cout << "dist = " << shortestPath.dist << endl;
	//city_dij++;
      }//if
    }//for
    //check if all cities are completed
    for(int i = city_completed; i < cityCount; i++)
    {
      if(mapNew[cityPos[i].row][cityPos[i].col].known == false)
      {
	      //cout << "foundAll = false" << endl;
	      break;
      }

      if(i == cityCount - 1)
      {
	      //cout << "foundAll = true " << endl;
        foundAll = true;
      }//if
    }//for
    
  }//while cities haven't been found yet
  dij.makeEmpty();
  //cout << "empty heap" << endl;
}//dijketra

//Router::backtrack(Coordinates current, 

void Router::resetMap()
{
  //cout << "reseting map" << endl;
  for(int r = 0; r < mapWidth; r++)
  {
    for(int c = 0; c < mapWidth; c++)
    {
      mapNew[r][c].dist = INT_MAX;
      mapNew[r][c].known = false;
    }
  }
  //cout << "finish reseting map" << endl; 
}


