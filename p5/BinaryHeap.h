        #ifndef _BINARY_HEAP_H_
        #define _BINARY_HEAP_H_

        #include "dsexceptions.h"
        #include "vector.h"
//	#include "RouterRunner.h"
	#include "router.h"
        // BinaryHeap class
        //
        // CONSTRUCTION: with a negative infinity sentinel and
        //               optional capacity (that defaults to 100)
        //
        // ******************PUBLIC OPERATIONS*********************
        // void insert( x )       --> Insert x
        // deleteMin( minItem )   --> Remove (and optionally return) smallest item
        // Comparable findMin( )  --> Return smallest item
        // bool isEmpty( )        --> Return true if empty; else false
        // bool isFull( )         --> Return true if full; else false
        // void makeEmpty( )      --> Remove all items
        // ******************ERRORS********************************
        // Throws Underflow and Overflow as warranted
/*
class Vertex // 
{
  public:
    Coordinates position;
    int index;//vertex number
    int dist; // distance
    bool known; 
    Coordinates previous;
    int edges[4];// 0 = north, 1 = east, 2 = south, 3 = west
};
*/
class BinaryHeap
{
  public:
     explicit BinaryHeap( int capacity = 350 * 350 );
     bool isEmpty( ) const;
     //bool isFull( ) const;
     const Vertex & findMin( ) const;
     void insert( Vertex & x );
     void deleteMin( );
     void deleteMin( Vertex & minItem );
     void makeEmpty( );

  private:
    int currentSize;  // Number of elements in heap
    Vertex array[10000];        // The heap array
    void buildHeap( );
    void percolateDown( int hole );
    void copy(Vertex &x1, Vertex &x2);
 };

//#include "BinaryHeap.cpp"
#endif
