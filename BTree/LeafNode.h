#ifndef LeafNodeH
#define LeafNodeH

#include "BTreeNode.h"

class LeafNode:public BTreeNode
{
  int *values;
public:
  LeafNode(int LSize, InternalNode *p, BTreeNode *left, BTreeNode *right);
  int getMinimum() const;
  int getMax() const;
  LeafNode* insert(int value); // returns pointer to new Leaf if splits
  void print(Queue <BTreeNode*> &queue);
  void insertNotFull(int value);
  LeafNode* insertFull(int value);
  void sort();
  bool checkFull();
  bool checkEven();
}; // LeafNode class

#endif
