#include <iostream>
#include "InternalNode.h"

using namespace std;

InternalNode::InternalNode(int ISize, int LSize,
  InternalNode *p, BTreeNode *left, BTreeNode *right) :
  BTreeNode(LSize, p, left, right), internalSize(ISize)
{
  keys = new int[internalSize]; // keys[i] is the minimum of children[i]
  children = new BTreeNode* [ISize];
} // InternalNode::InternalNode()

int InternalNode::getMinimum()const
{
  if(count > 0) // should always be the case
    return children[0]->getMinimum();
  else
    return 0;
} // InternalNode::getMinimum()

int InternalNode::getMax()const
{
  if(count > 0)
    return keys[count - 1];
  else
    return 0;
} // getMax()

InternalNode* InternalNode::insert(int value)
{
  // cout << "InternalNode::insert called" << endl;
  BTreeNode *newNode = NULL;
  sort();
  for(int i = 0; i < count; i++)
  {
    // cout << "keys[i] =" << keys[i] << endl;
    // cout << "value = " << value << endl;

    if(value < keys[i]) // check if left leaf is full
    {
      // cout << "InternalNode::insert if statement called" << endl;
      if((i - 1) >= 0) // if not then insert in left one
      {  
        newNode = children[i-1]->insert(value);
        sort();
        getKeys();
        break;
      } // if i-1 != 0
      else
      {
        newNode = children[i]->insert(value);
        sort();
        getKeys();
        break;
      } // else i -1 >= 0 happens when i == 0
    } // if(value < keys[i])
    else if(i + 1 == count) // else if value is greater than all keys
    {
      newNode = children[i]->insert(value);
      sort();
      getKeys();
      break;
    }
  }
  
  if(newNode)
  {
    return insert(newNode);
  }

  return NULL; // to avoid warnings for now.
} // InternalNode::insert(value)


void InternalNode::insert(BTreeNode *oldRoot, BTreeNode *node2) // to split
{ // Node must be the root, and node1
  keys[count] = oldRoot->getMinimum();
  children[count] = oldRoot;
  count++;
  keys[count] = node2->getMinimum();
  children[count] = node2;
  node2->setParent(this);
  oldRoot->setParent(this);
  count++;
} // InternalNode::insert()


InternalNode* InternalNode::insert(BTreeNode *newNode) // from a sibling
{
  if(!(checkFull()))
  {
    insertNotFull(newNode);
    return NULL;
  }
  else
  {
    return insertFull(newNode);
  }
} // InternalNode::insert()

/*********************ADDED FUNCTIONS*********************/

bool InternalNode::checkFull(){return (count == internalSize);}

void InternalNode::insertNotFull(BTreeNode *newNode)
{
  // cout << "BTreeNode::insertNotFull(): " << endl;
  children[count] = newNode;
  count++;
  this->sort();
} // insert not full

InternalNode* InternalNode::insertFull(BTreeNode *newNode)
{
  // cout << "insertFull():" << endl;
  /*********************** This is to check left side *************************/
  BTreeNode *left_check = getLeftSibling();
  BTreeNode *right_check = getRightSibling();
  
  // checks whether the left size has another space to add another internal node
  if(left_check) //check if left size exists
  {
    if(left_check->getCount() != internalSize) // check left size is not full
    {
      if(getMinimum() < newNode->getMinimum()) // check whether the minimum on this node is less than value
      {
        dynamic_cast<InternalNode*>(left_check)->insert(children[0]);
        // need to insert value into values so that the minimum is gone and it is sorted
        children[0] = newNode;
        keys[0] = newNode->getMinimum();
        this->sort();
        this->getKeys();
      }
      else if(getMinimum() > newNode->getMinimum())
      {
        // cout << "left_btree insert minimum -- value:  " << left_check->getCount() << endl;
        dynamic_cast<InternalNode*>(left_check)->insert(newNode);
        this->sort();
        this->getKeys();
      }
      return NULL;
    } // if
  } // if
  
  if(right_check) // check right side exists
  {
    if(right_check->getCount() != internalSize)
    {
      if(keys[count - 1] < newNode->getMinimum()) // check whether the max on this leaf is more than value
      {
        // cout << "right_btree insert minimum -- count:  " << right_check->getCount() << endl;
        dynamic_cast<InternalNode*>(right_check)->insert(newNode);
        this->sort();
      }
      else
      {
        // cout << "right_btree insert value -- count:  " << right_check->getCount() << endl;
        dynamic_cast<InternalNode*>(right_check)->insert(children[count - 1]);
        children[count - 1] = newNode;
        keys[count - 1] = newNode->getMinimum();
        this->sort();
        this->getKeys();
      }
      return NULL;
    } // if
  } // if
  // else if the leaf is the very right

  /******************* Split the node *********************/

  int done = 0;
    int j;
    InternalNode *split = new InternalNode(internalSize, leafSize, NULL, this, right_check);
    this->setRightSibling(split);
    if(internalSize % 2)
      j = internalSize / 2 + 1;
    else
      j = internalSize / 2;
    for(int i = j; i < internalSize; i++)
    {
      if(!done)
      {
        this->insert(newNode);
        done = 1;
      } // if
      count--;
      split->insert(children[i]);
      this->getKeys();
    } // for
    
  return split;
  
} // insertFull


void InternalNode::sort()
{
  BTreeNode *temp;
  int i, j, temp_key;
  this->getKeys();
  for(i = 1; i < count; i++)
  {
    temp = children[i];
    temp_key = keys[i];
    for(j = i - 1; j >= 0 && temp_key < keys[j]; j--)
    {
      children[j + 1] = children[j];
      keys[j + 1] = keys[j];
    } // for(j)
    children[j + 1] = temp;
    keys[j + 1] = temp_key;
  } // for(i)
  for(i = 0; i < count; i++) // this is to reorganize the nodes
  {
    if(i == 0)
      children[i]->setLeftSibling(NULL);
    else
      children[i]->setLeftSibling(children[i - 1]);

    if(i + 1 == count)
      children[i]->setRightSibling(NULL);
    else
      children[i]->setRightSibling(children[i + 1]);
  } // for
  this->getKeys();
  if(this->getLeftSibling())
    dynamic_cast<InternalNode*>(this->getLeftSibling())->getKeys();
  if(this->getRightSibling())
    dynamic_cast<InternalNode*>(this->getRightSibling())->getKeys();
}


void InternalNode::getKeys()
{
  for(int k = 0; k < count; k++)
    keys[k] = children[k]->getMinimum();
} // update keys

void InternalNode::print(Queue <BTreeNode*> &queue)
{
  int i;

  cout << "Internal: ";
  for (i = 0; i < count; i++)
    cout << keys[i] << ' ';
  cout << endl;

  for(i = 0; i < count; i++)
    queue.enqueue(children[i]);

} // InternalNode::print()

