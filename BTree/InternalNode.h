#ifndef InternalNodeH
#define InternalNodeH

#include "BTreeNode.h"

class InternalNode:public BTreeNode
{
  int internalSize;
  BTreeNode **children;
  int *keys;
public:
  InternalNode(int ISize, int LSize, InternalNode *p, 
    BTreeNode *left, BTreeNode *right);
  int getMinimum()const;
  int getMax()const;
  InternalNode* insert(int value); // returns pointer to new InternalNode
  void insert(BTreeNode *oldRoot, BTreeNode *node2); // if root splits use this
  InternalNode* insert(BTreeNode *newNode); // from a sibling
  bool checkFull();
  void insertNotFull(BTreeNode *newNode);
  InternalNode* insertFull(BTreeNode *newNode);
  void getKeys();
  void sort();
  void print(Queue <BTreeNode*> &queue);

}; // InternalNode class

#endif
