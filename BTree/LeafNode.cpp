#include <iostream>
#include "LeafNode.h"
#include "InternalNode.h"
#include "QueueAr.h"

using namespace std;


LeafNode::LeafNode(int LSize, InternalNode *p,
  BTreeNode *left, BTreeNode *right) : BTreeNode(LSize, p, left, right)
{
  values = new int[LSize];
}  // LeafNode()


int LeafNode::getMinimum()const
{
  if(count > 0)  // should always be the case
    return values[0];
  else
    return 0;

} // LeafNode::getMinimum()


int LeafNode::getMax()const
{
  if(count > 0)
    return values[count - 1]; // should always be the case
  else
    return values[0];
}


LeafNode* LeafNode::insert(int value)
{
  if(!checkFull())
  {
    insertNotFull(value);
    return NULL;
  }
  else
  {
      return insertFull(value);
  }
}  // LeafNode::insert()


bool LeafNode::checkFull()
{
  return count == leafSize;
}


bool LeafNode::checkEven()
{
  return (leafSize % 2 == 0);
}


void LeafNode::insertNotFull(int value)
{
  values[count] = value;
  count++;
  this->sort();
}//insertNotFull

LeafNode* LeafNode::insertFull(int value)
{
  // cout << "insertFull():" << endl;
  
  /*********************** This is to check left side *************************/
  BTreeNode *left_check = getLeftSibling();
  BTreeNode *right_check = getRightSibling();
  //checks if the left size is full
  if(left_check) // check if left size exists
  {
    // cout << "left BTree exists" << endl;
    if(left_check->getCount() != leafSize) // check left size is not full
    {
      if(getMinimum() < value) // check whether the minimum on this leaf is less than value
      {
        // cout << "left_btree insert minimum -- count:  " << left_check->getCount() << endl;
        left_check->insert(getMinimum()); // insert minimum to left sibling
        // need to insert value into values so that the minimum is gone and it is sorted
        values[0] = value;
        sort();
      }
      else if(getMinimum() > value)
      {
        // cout << "left_btree insert minimum -- value:  " << left_check->getCount() << endl;
        left_check->insert(value);
      }
      return NULL;
    } // if
  } // if
  if(right_check) // check right side exists
  {
    if(right_check->getCount() != leafSize)
    {
      if(getMax() > value) // check whether the max on this leaf is more than value
      {
        // cout << "right_btree insert minimum -- count:  " << right_check->getCount() << endl;
        right_check->insert(getMax()); // insert minimum to left sibling
        // need to insert value into values so that the minimum is gone and it is sorted
        values[count - 1] = value;
        sort();
      }
      else
      {
        // cout << "right_btree insert value -- count:  " << right_check->getCount() << endl;
        right_check->insert(value);
      }
      return NULL;
    } // if
  } // if
  
  // else//if the leaf is the very right
  LeafNode *right = new LeafNode(leafSize, NULL, this, getRightSibling());
  this->setRightSibling(right);
  int right_count = 0; // how many elements are in *right
  int this_pos = leafSize - 1; // index of current leaf
  bool value_entered = false; // we already entered the value

  {
    // cout << "Splitting node" << endl;
    while(this_pos >= 0 && right_count != (leafSize/2 + 1)) // creates a new right array
    {
        if(values[this_pos] > value || value_entered) // check if value is greater than int in array
        {
          right->insert(values[this_pos]);
          this_pos--;
          right_count++;
          count--;
          // cout << "count--: " << count << endl;
        }
        else // if value is greater than values(this_pos)
        {
          right->insert(value);
          right_count++;
          value_entered = true;
        } // else

      if(right_count == (leafSize/2 + 1) && !(value_entered))
      {
        // cout << "Inserted value into left leaf" << endl;
        insert(value);
      }
    } // while
  } // else

  return right;

}


void LeafNode::sort()
{
  int i, j, temp;
  for(i = 1; i < count; i++)
  {
    temp = values[i];
    for(j = i - 1; j >= 0 && temp < values[j]; j--)
    {
      values[j + 1] = values[j];
    } // for(j)
    values[j + 1] = temp;
  } // for(i)
} // sort


void LeafNode::print(Queue <BTreeNode*> &queue)
{
  cout << "Leaf: ";
  for (int i = 0; i < count; i++)
    cout << values[i] << ' ';
  cout << endl;
} // LeafNode::print()


