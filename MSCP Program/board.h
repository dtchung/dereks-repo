//Alton Fong, Derek Chung
#ifndef BOARD_H
#define BOARD_H

//typedef unsigned char byte;

class Board
{
public:
  Board(){setmem();}
  ~Board() {/*empty body*/}
  void print_board(int);
  unsigned char& operator[](int i) { return cboard[i]; }
  void print_square(int);
  void setmem(void);
private:
  unsigned char cboard[64+3];


};

#endif
