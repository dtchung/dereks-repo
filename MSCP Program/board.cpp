#include "board.h"

#include <cstdio>
#include <cstring>
#include <iostream>

const int CASTLE_WHITE_KING  = 1;
const int CASTLE_WHITE_QUEEN = 2;
const int CASTLE_BLACK_KING  = 4;
const int CASTLE_BLACK_QUEEN = 8;
enum {
	EMPTY,
	WHITE_KING, WHITE_QUEEN, WHITE_ROOK,
	WHITE_BISHOP, WHITE_KNIGHT, WHITE_PAWN,
	BLACK_KING, BLACK_QUEEN, BLACK_ROOK,
	BLACK_BISHOP, BLACK_KNIGHT, BLACK_PAWN
};
enum {                  /* 64 squares */
        A1, A2, A3, A4, A5, A6, A7, A8,
        B1, B2, B3, B4, B5, B6, B7, B8,
        C1, C2, C3, C4, C5, C6, C7, C8,
        D1, D2, D3, D4, D5, D6, D7, D8,
        E1, E2, E3, E4, E5, E6, E7, E8,
        F1, F2, F3, F4, F5, F6, F7, F8,
        G1, G2, G3, G4, G5, G6, G7, G8,
        H1, H2, H3, H4, H5, H6, H7, H8,
        CASTLE,         /* Castling rights */
        EP,             /* En-passant square */
        LAST            /* Ply number of last capture or pawn push */
};
enum { FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H };
enum { RANK_1, RANK_2, RANK_3, RANK_4, RANK_5, RANK_6, RANK_7, RANK_8 };

inline char PIECE2CHAR(int p)    {return ("-KQRBNPkqrbnp"[p]);}    /* piece to text */

inline int F(int square)         {return ((square) >> 3);}         /* file */
inline int R(int square)         {return ((square) & 7);}          /* rank */
inline int SQ(int f, int r)      {return (((f) << 3) | (r));}    /* compose square */
inline char RANK2CHAR(int r)     {return ('1'+(r));}               /* rank to text */
inline char FILE2CHAR(int f)     {return ('a'+(f));}               /* file to text */
inline int MOVE(int fr, int to)  {return (((fr) << 6) | (to));}    /* compose move */
const int SPECIAL               = (1<<12);                 /* for special moves */

using namespace std;

void Board::print_board(int ply)
{
        int file, rank;

        for (rank=RANK_8; rank>=RANK_1; rank--) {
                cout << (1 + rank) << " ";
                for (file=FILE_A; file<=FILE_H; file++) {
                        putchar(' ');
                        putchar(PIECE2CHAR(cboard[SQ(file,rank)]));
                }
                putchar('\n');
        }
        cout << "   a b c d e f g h" << endl;
        cout << (1+ply/2) << ". ";
        cout << ((~ply & 1) ? "White" : "Black") << " to move. ";
        cout << (cboard[CASTLE] & CASTLE_WHITE_KING ? "K" : "");
        cout << (cboard[CASTLE] & CASTLE_WHITE_QUEEN ? "Q" : "");
        cout << (cboard[CASTLE] & CASTLE_BLACK_KING ? "k" : "");
        cout << (cboard[CASTLE] & CASTLE_BLACK_QUEEN ? "q" : "");
        cout << " ";
       
        if (cboard[EP]) print_square(cboard[EP]);
        putchar('\n');
}

void Board::print_square(int square)
{
        putchar(FILE2CHAR(F(square)));
        putchar(RANK2CHAR(R(square)));
}

void Board::setmem(void)
{
  memset(cboard, 0, sizeof(unsigned char) * 67);
}

