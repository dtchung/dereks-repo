# This class will compare files based on the preference of the user
#
# Recieves 5 parameters:
# Name: would be a string that contains the file's name
# headerline: a boolean that shows whether there is a header file that shows
#             headerline or not. A headerline contains two numbers: the version
#             number, and the number of lines. If true, program will read in the
#             number of lines and compare it with the actually number of lines that
#             it reads in. 
# verbose: is a boolean. True: prints out error and each individual comparison
#           that is greater than the specified max error. False, only prints out number
#           of errors
# line_skip: number of lines to skip
#
# comparisons: A double lies that holds vectors with two spots. In each vector, there will
#              be a string that specifies what type of comparisons there is to be. There
#              will be a number that represents the specified max error.
#Example:
#    mb_file = compare_class.FileCompare("file_name", True, False, 1, [["RelativeDiff", 1e-2], ["AbsDiff", 1e-2], ["VecDiff2D", 1e-2]])


class FileCompare(object):
    def __init__(self, name, headerline, verbose, line_skip, comparisons):
        self.name = name
        self.headerline = headerline
        self.line_skip = line_skip
        self.comparisons = comparisons
        self.error = 0
        self.count_line = 0
        self.verbose = verbose
        
        self.file = self.read_file()

    
#    This function would read a file and store them into lists.
    
    def read_file(self):
        
        infile = open(self.name, 'r')

#       Skips the number of specified lines
        for x in range(self.line_skip):
            infile.readline()

#       reads in the header
        if self.headerline:
            version, num_of_lines = [int(x) for x in infile.readline().split()]
        
        self.count_line = 0
        data= {}
        temp = {}
        #uses first line to find how many columns there needs to be
        for temp_index, first_line_value in enumerate(infile.readline().split()):
            temp[temp_index] = [float(first_line_value)]

        self.count_line += 1

        
        
        #fills up the lists/reads lines
        for line in infile:
            row = line.split()
            for temp_index, value in enumerate(row):
                temp[temp_index].append(float(value))
            self.count_line+= 1

        #adds to there error the difference of the read in lines and num_of_lines
        if self.headerline:
            self.error += abs(self.count_line - int(num_of_lines))
        
        temp_index = 0
        for data_index, comp_list in enumerate(self.comparisons):
            if comp_list[0] == "VecDiff2D" or comp_list[0] == "CrossCorr":
                #map(list,zip(charlist,numlist))
                data[data_index] = map(list, zip(temp[temp_index], temp[temp_index + 1]))
                temp_index += 2                
            else:
                data[data_index] = temp[temp_index]
                temp_index += 1
 
        return data

    #compares 2 files
    def compare(self, file_b):
        if file_b.comparisons != self.comparisons:
            return 0
        #loop through lists
        for lists in self.file:
            # loop fo each index/value pair
            for index, a_value in enumerate(self.file[lists]):
                #absolute difference
                if self.comparisons[lists][0] == "AbsDiff":
                    error = abs(a_value - file_b.file[lists][index])
                    if error > self.comparisons[lists][1]:
                        self.error += 1
                        if self.verbose:
                            print str(a_value) + " and " + str(file_b.file[lists][index]) +" has an AbsDiff of " + str(error)
                #relative difference
                elif self.comparisons[lists][0] == "RelativeDiff":
                    if abs(a_value) >= abs(file_b.file[lists][index]):
                        denominator = abs(a_value)
                        numerator = abs(a_value - file_b.file[lists][index])
                    else:
                        denominator = abs(file_b.file[lists][index])
                        numerator = abs(a_value - file_b.file[lists][index])
                    #no error if both 0
                    if denominator != 0:
                        error = numerator/denominator
                        if error > self.comparisons[lists][1]:
                            self.error += 1
                            if self.verbose:
                                print str(a_value) + " and " + str(file_b.file[lists][index]) + " has a RelativeDiff of " + str(error)
                #Vector Difference
                elif self.comparisons[lists][0] == "VecDiff2D":
                    error = abs((a_value[0]**2 + a_value[1]**2)**.5 - (file_b.file[lists][index][0]**2 + file_b.file[lists][index][1]**2)**.5)
                    if error > self.comparisons[lists][1]:
                        self.error += 1
                        if self.verbose:
                            print str(a_value) + " and " + str(file_b.file[lists][index]) + " has a Vector difference of " + str(error)                

        
        for list_index, comp_list in enumerate(self.comparisons):
            if comp_list[0] == "CrossCorr":
                a_sum = 0
                b_sum = 0
                #a_value would be a double list
                #this loop would find the sum
                for vector_index, a_vector in enumerate(self.file[list_index]):
                    a_sum += a_vector[1]
                    b_sum += file_b.file[list_index][vector_index][1]
                #find the mean
                a_mean = a_sum/self.count_line
                b_mean = b_sum/self.count_line
                #find deviation
                a_deviation = 0
                b_deviation = 0
                for vector_index, a_vector in enumerate(self.file[list_index]):
                    a_deviation += (a_vector[1] - a_mean)**2
                    b_deviation += (file_b.file[list_index][vector_index][1] - b_mean)**2
                a_deviation = (a_deviation/self.count_line)**.5
                b_deviation = (b_deviation/self.count_line)**.5
                
                #to find cross correlation
                cross = 0
                for vector_index, a_vector in enumerate(self.file[list_index]):
                    cross += ((a_vector[1] - a_mean) * (file_b.file[list_index][vector_index][1] - b_mean))/(a_deviation * b_deviation)

                cross = cross/self.count_line

                #checks cross with max error
                if (abs(cross - 1) > comp_list[1]):
                    self.error += 1
                    if self.verbose:
                        print "The Cross Correlation is " + str(cross)

                
                # We add 1 to error if the time is off
                time_check = True
                for vector_index, a_vector in enumerate(self.file[list_index]):
                    if time_check and (a_vector[0] != file_b.file[list_index][vector_index][0]):
                        self.error += 1
                    
        return self.error
        
                    

