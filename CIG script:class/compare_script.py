#!/usr/bin/python
import compare_class
import glob


file_list = glob.glob("results_macbook/regtest.botm.*.*")
for file_name in file_list:
    fn = file_name.split("/")[1]
    print fn
    mb_file = compare_class.FileCompare("results_macbook/" + fn, True, False, 0, [["RelativeDiff", 1e-2], ["AbsDiff", 1e-2], ["VecDiff2D", 1e-3]])
    ym_file = compare_class.FileCompare("results_ymir/" + fn, True, False, 0, [["RelativeDiff", 1e-2], ["AbsDiff", 1e-2], ["VecDiff2D", 1e-3]])
    print str(mb_file.compare(ym_file))

file_list = glob.glob("results_macbook/regtest.surf.*.*")
for file_name in file_list:
    fn = file_name.split("/")[1]
    print fn
    mb_file = compare_class.FileCompare("results_macbook/" + fn, True, False, 0, [["RelativeDiff", 1e-2], ["AbsDiff", 1e-2], ["VecDiff2D", 1e-2]])
    ym_file = compare_class.FileCompare("results_ymir/" + fn, True, False, 0, [["RelativeDiff", 1e-2], ["AbsDiff", 1e-2], ["VecDiff2D", 1e-2]])
    print str(mb_file.compare(ym_file))


file_list = glob.glob("results_macbook/regtest.visc.*.*")
for file_name in file_list:
    fn = file_name.split("/")[1]
    print fn
    mb_file = compare_class.FileCompare("results_macbook/" + fn, True, False, 0, [["RelativeDiff", 1e-2]])
    ym_file = compare_class.FileCompare("results_ymir/" + fn, True, False, 0, [["RelativeDiff", 1e-2]])
    print str(mb_file.compare(ym_file))

file_list = glob.glob("results_macbook/regtest.velo.*.*")
for file_name in file_list:
    fn = file_name.split("/")[1]
    print fn
    mb_file = compare_class.FileCompare("results_macbook/" + fn, True, False, 1, [["RelativeDiff", 1e-2], ["AbsDiff", 1e-2], ["VecDiff2D", 1e-2]])
    ym_file = compare_class.FileCompare("results_ymir/" + fn, True, False, 1, [["RelativeDiff", 1e-2], ["AbsDiff", 1e-2], ["VecDiff2D", 1e-2]])
    print str(mb_file.compare(ym_file))

print "mac_seismogram"
mb_file = compare_class.FileCompare("seismograms/mac_seismogram", False, False, 0, [["CrossCorr", 1e-5]])
ym_file = compare_class.FileCompare("seismograms/ymir_seismogram", False, False, 0, [["CrossCorr", 1e-5]])
print str(mb_file.compare(ym_file))
