#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int get_choice(void);
void display_info(int* crn, char** course_number, char** subject, int count);
void find_CRN(int* crn, char** course_number, char** subject, int count);
void find_subject(int* crn, char** course_number, char** subject, int count);
void read_courses(char* file, int** crn, char*** course_number, char*** subject, int *count);
void initialize(int** crn, char*** course_number, char*** subject, int count);
void resize(int** crn, char*** course_number, char*** subject, int *count);
void deallocate(int** crn, char*** course_number, char*** subject, int count);


