//Derek Chung
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"

void initialize(int** crn, char*** course_number, char*** subject, int count)
{
  int i;

  (*crn) = malloc(count * sizeof(int));
  (*course_number) = (char**)malloc(count * sizeof(char*));
  (*subject) = (char**)malloc(count * sizeof(char*));
 
  for(i = 0; i < count; i++)
  {
    (*course_number)[i] = (char*)malloc(4 * sizeof(char));
    (*subject)[i] = (char*)malloc(6 * sizeof(char));
  }//for
}//mallocs the arrays that are passed in to 100




void resize(int** crn, char*** course_number, char*** subject, int* count)
{
  int* new_crn = malloc(*count * sizeof(int) * 2);
  char** new_course_number = (char**)malloc(*count * sizeof(char*) * 2);
  char** new_subject = (char**)malloc(*count * sizeof(char*) * 2);
  int i;

  //dynamically allocates new_course_number and subject
  for(i = 0; i < *count * 2; i++)
  {
    new_course_number[i] = (char*)malloc(4);
    new_subject[i] = (char*)malloc(6);
  }//for
  
  /*
   * loops through old array and copies to new array. 
   */ 
  for(i = 0; i < *count; i++)
  {
    new_crn[i] = (*crn)[i];
    strcpy(new_course_number[i], (*course_number)[i]);
    strcpy(new_subject[i], (*subject)[i]);
  }//for
  
  /* dealloca thre array */

  deallocate(crn, course_number, subject, *count);
  
  *crn = new_crn;
  *course_number = new_course_number;
  *subject = new_subject;
  
  *count = (*count) * 2;
}//doubles the size of the arrays that are passed in

void deallocate(int** crn, char*** course_number, char*** subject, int count)
{
  int i;

  free(*crn);
  
  //free the array elements for course_number and subject
  for(i = 0; i < count; i++)
  {
    free((*course_number)[i]);
    free((*subject)[i]);
  }//for

  free(*course_number);
  free(*subject);
}//deallocates the arrays that are passed in
 
