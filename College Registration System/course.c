//Derek Chung
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "course.h"
#include "vector.h"

void read_courses(char* file, int** crn, char*** course_number, char*** subject, int* count)
{
  
  char* tok;
  int crn_num;
  int max_count;
  FILE *fp = NULL;
  char current_line[80];
  int index = 0;
 
  if(!(fp = fopen(file, "r")))
  {
    printf("Can't open textdata\n");
    exit(1);
  }//if file cannot open

  initialize(crn, course_number, subject, *count);
   
  max_count = *count;

  while(fgets(current_line, 80, fp))
  {
    tok = strtok(current_line, "\t");
    if(tok == NULL)
    { 
      continue;
    }//if tok doesn't point to anything
    
    crn_num = atoi(tok);
    
    if(crn_num > 9999 && crn_num < 99999)
    {
      (*crn)[index] = crn_num;
      tok = strtok(NULL, "\t");

      if(tok[0] == '^')
        tok = strtok(NULL, "\t");
      //if gets rid of the ^

      strcpy((*subject)[index], tok);
      tok = strtok(NULL, "\t");
      strcpy((*course_number)[index], tok);
      index++;
    }//if value is 5 digits long
   
    if(index >= max_count)
    {
      resize(crn, course_number, subject, count);
      max_count = *count;
    }//if index excedes size of arrays
  }//while reaches the end of file
  
}
/*
 * Reads in a pointer to argv[] and arrays crn and array pointers
 * course_numbers subject and count. It will open file and if it
 * can't open, then it will print to the monitor and notify users that 
 * the file cannot open. It will parse through the html file and find
 * a CRN number. Later the function will pass in values to CRN and 
 * strings into subject and course_number.
 */

void find_CRN(int* crn, char** course_number, char** subject, int count)
{ 
  int entered_crn;
  int valid = 0;
  int i;
  printf("Please enter a CRN: ");
  scanf("%d", &entered_crn);
  
  for(i = 0; i < count; i++)
  {
    if(crn[i] == entered_crn)
    {
      valid = 1;
      break;
    }//if entered crn is the same value as crn[]
  }//for loops through whole array until crn == crn[]

  if(valid)//if value of crn exists in CRN[]
    printf("%s %s\n", subject[i], course_number[i]);
  else//else tells user that crn doesn't exists in CRN[]
    printf("CRN %d not found.\n", entered_crn);
}/* 
  * search through value entered by user and compares it with CRN array
  * and later finds the same index. Then it will print the strings in
  * the index of the subject and course_number. If there is no CRN 
  */

void find_subject(int* crn, char** course_number, char** subject, int count)
{
  char entered_sub[10];
  int valid = 0;
  int i;

  printf("Please enter a subject: ");
  scanf("%10s", entered_sub);
  
  for(i = 0; i < count; i++)
  {
    if(strcmp(subject[i], entered_sub) == 0)
    {
      valid = 1;
      printf("%d %s %s\n", crn[i], subject[i], course_number[i]);
    }//if subject == entered sub
  }//for loops through all of the subject array
   
  if(!valid)//if there entered string doesn't match anything in subject
    printf("No courses were found for %s\n", entered_sub);
}/*
  * Asks the user to enter a subject and this function checks through the
  * subject array and if it matches, it prints out the CRN and course_number
  * of the index of the variable that matches the entered string in subject.
  * if there is nothing, then it will notify users that no courses were found.
  */
