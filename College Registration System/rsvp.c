//Derek Chung
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "course.h"
#include "vector.h"

void display_info(int* crn, char** course_number, char** subject, int count);
int get_choice(void);

int main(int argc, char *argv[])
{
  char *file = argv[1];
  int *crn = NULL;
  int count = 100;
  char **course_number = NULL, **subject = NULL;  

  read_courses(file, &crn, &course_number, &subject, &count);
  display_info(crn, course_number, subject, count);
  deallocate(&crn, &course_number, &subject, count);
  return 0;
}/*
  * reads in a file and instantiate variables and call functions
  */

int get_choice(void)
{
  int choice;
  do
  {
    printf("\nRSVP Menu\n");
    printf("0. Done.\n");
    printf("1. Find by CRN.\n");
    printf("2. Find by subject.\n");
    printf("Your choice (0 - 2): ");
    scanf("%d", &choice);
    
    if(choice > 2 || choice < 0)//if user entered a number out of range
      printf("Your choice is outside the acceptable range.  Please try again.\n");

    
  }while(choice > 2 || choice < 0);//while user did not enter a valid number
  
  return choice;
}//promps user to enter a number and returns what the user entered 
 //if in valid range

void display_info(int* crn, char** course_number, char** subject, int count)
{
  int choice;

  do
  {
    choice = get_choice();
     
    if(choice == 1)//if
      find_CRN(crn, course_number, subject, count);
    
    else if(choice == 2)//if
      find_subject(crn, course_number, subject, count);

  }while(choice != 0);//
}/* 
  * reads in a value and 3 arrays and later exectute functions based on
  * what the value is
  */


