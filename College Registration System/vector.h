//Derek Chung
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void initialize(int** crn, char*** course_number, char*** subject, int count);

void resize(int** crn, char*** course_number, char*** subject, int* count);

void deallocate(int** crn, char*** course_number, char*** subject, int count);

